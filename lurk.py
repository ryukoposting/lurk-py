#!/usr/bin/env python
import sys
sys.path.insert(0, 'api')

from chan import *

def main():
    beginThreadMonitor()
    print('\n*************************************')
    print('*        4CHAN IMAGE SCRAPER        *')
    print('*************************************')
    print('To add a thread to the scraper, enter')
    print('the board, followed by the thread')
    print('number (the OP\'s post number)')
    print('example: for /g/ thread 12345, user should enter "g 12345"')
    while True:
        add = input('> ').split()
        if len(add) != 2:
            print('invalid entry')
        else:
            addThreadToMonitor(add[0], add[1])

if __name__ == "__main__":
    main()
