#!/usr/bin/env python
import requests
import threading
import urllib.request
import os
import hashlib
import filecmp
import time
from time import sleep

# list of tuples, (board thread number_of_images_retrieved)
threadList = list()

def threadsMonitor():
    global threadList
    while True:
        for thread in threadList:
            #print("Thread Monitor: updating", thread[0], thread[1])
            currentTime = time.clock()
            downloadNew(thread)
            sleep(0 if (time.clock() - currentTime) > 10 else (time.clock() - currentTime))

def beginThreadMonitor():
    thread = threading.Thread(target=threadsMonitor, args=())
    thread.daemon = True
    thread.start()

def addThreadToMonitor(board, thread):
    global threadList
    threadList.append((board, thread, 0))

def hashfile(fname):
    file = open(fname, 'rb')
    hasher = hashlib.md5()
    buffer = file.read(65536)
    while len(buffer) > 0:
        hasher.update(buffer)
        buffer = file.read(65536)
    file.close()
    return hasher.hexdigest()

def compareTempFileUsingHash(fname):
    temphash = hashfile('temp/' + fname)
    for dlfile in os.listdir('download/'):
        if temphash == hashfile('download/' + dlfile):
            os.remove('temp/' + fname)
            return None
    os.rename('temp/' + fname, 'download/' + fname)

def compareTempFile(fname):
    with open('temp/' + fname, 'rb') as tempfile:
        for dlfile in os.listdir('download/'):
            if tempfile.read() == open('download/' + dlfile, 'rb').read():
                os.remove('temp/' + fname)
                return None
    try:
        os.rename('temp/' + fname, 'download/' + fname)
    except FileExistsError:
        os.rename('temp/' + fname, 'download/_' + fname)
    except PermissionError:
        pass

def downloadNew(thread):
    global threadList
    url = 'https://a.4cdn.org/' + thread[0] + '/thread/' + thread[1] + '.json'
    r = requests.get(url = url)
    if r.status_code == 200:
        json = r.json()['posts']
        imgcount = 0
        for post in json:
            if 'filename' in post:
                imgcount += 1
                if imgcount >= thread[2]:
                    imageUrl = 'http://i.4cdn.org/' + thread[0] + '/' + str(post['tim']) + post['ext']
                    #print('Downloading:', imageUrl)
                    urllib.request.urlretrieve(imageUrl, 'temp/' + post['filename'] + post['ext'])
                    compareTempFileUsingHash(post['filename'] + post['ext'])
        threadList.remove(thread)
        threadList.append((thread[0], thread[1], json[0]['images']))
    else:
        threadList.remove(thread)
        return -1
